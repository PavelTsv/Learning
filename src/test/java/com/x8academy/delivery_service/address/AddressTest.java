package com.x8academy.delivery_service.address;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;


/**
 * Test cases for class Address.
 */
public class AddressTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void givenAppNumberBelowZeroWhenAddressInstantiatedThenThrowAssertionError() {
        exception.expect(AssertionError.class);
        final Address sofiaAddress = new Address(Country.Bulgaria, City.Sofia,
                "street name", 4, -4);
    }

    @Test
    public void givenStreetNumberBelowOrEqualToZeroWhenAddressInstantiatedThenThrowAssertionError() {
        exception.expect(AssertionError.class);
    }

}