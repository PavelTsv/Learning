package com.x8academy.delivery_service.person;

/**
 * Created by pavel on 05.10.17.
 */
public enum Sex {
    /**
     * Male gender.
     */
    MALE,
    /**
     * Female gender.
     */
    FEMALE
}
