package com.x8academy.delivery_service.person;

import com.x8academy.delivery_service.address.Address;
import com.x8academy.delivery_service.address.City;
import com.x8academy.delivery_service.address.Country;
import com.x8academy.delivery_service.company.DeliveryCompany;
import com.x8academy.delivery_service.company.Item;

/**
 * Imitates real life human being. The person has a name, family name, age, sex,
 * address and wealth.
 */
public class Person {
    private final String name;
    private final String familyName;
    private final int age;
    private final Sex sex;
    private final Address address;
    private final long wealth;

    /**
     * Default constructor for Person.
     */
    public Person() {
        //take a look
        this.name = "Nameless";
        this.familyName = "One";
        this.age = 1;
        this.sex = Sex.MALE;
        this.address = new Address(Country.Bulgaria, City.Sofia,
                "Dragan Tsankov", 1, 1);
        this.wealth = 0;
    }

    /**
     * Creates a Person object.
     * Name can't be null or empty and should meet the naming convention.
     * Family name can't be null or empty and should meet the naming convetion.
     * Age can't be below zero.
     * Sex should either be Male or Female.
     * Address can't be null.
     * Wealth can go below zero.
     *
     * @param name       name of the person.
     * @param familyName family name of the person.
     * @param age        the age of the person.
     * @param sex        the sex of the person.
     * @param address    the address o the person.
     * @param wealth     the amount of money the person has.
     */
    public Person(final String name, final String familyName, final int age,
                  final Sex sex, final Address address, final long wealth) {

        this.name = name;
        this.familyName = familyName;
        this.age = age;
        this.sex = sex;
        this.address = address;
        this.wealth = wealth;
    }

    /**
     * Makes a delivery to the given address.
     * deliveryCompany can't be null.
     * deliveryAddress can't be null.
     * Item can't be null and should be equal to ITEM.
     *
     * @param deliveryCompany the company which delivery's service will be used.
     * @param deliveryAddress the destination address.
     * @param item            the item to be delivered.
     */
    public void makeADeliveryTo(final DeliveryCompany deliveryCompany,
                                final Address deliveryAddress,
                                final Item item) {

    }
}
