package com.x8academy.delivery_service.person;

import java.time.DayOfWeek;
import java.util.List;

/**
 * Imitates a real world delivery job. Extends class Person.
 */
public final class DeliveryPerson extends Person {
    private final List<DayOfWeek> workingDays;
    private final long salary;

    /**
     * Creates a DeliveryPerson object that extends Person and works as a job
     * subclass of that class.
     * workingDays can't be empty or null.
     * salary should be above minimum wage.
     *
     * @param workingDays
     * @param salary
     */
    public DeliveryPerson(final List<DayOfWeek> workingDays,
                          final long salary) {

        this.workingDays = workingDays;
        this.salary = salary;
    }

    /**
     * Returns the working days of that person.
     *
     * @return {@link #workingDays}
     */
    public List<DayOfWeek> getWorkingDays() {
        return workingDays;
    }

    /**
     * Returns the salary of that person.
     *
     * @return {@link #salary}
     */
    public long getSalary() {
        return salary;
    }
}
