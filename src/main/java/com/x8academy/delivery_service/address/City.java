package com.x8academy.delivery_service.address;

/**
 * Created by pavel on 11.10.17.
 */
public enum City {
    /**
     * City Sofia in Country Bulgaria.
     */
    Sofia,
    /**
     * City Vidin in Country Bulgaria.
     */
    Vidin
}
