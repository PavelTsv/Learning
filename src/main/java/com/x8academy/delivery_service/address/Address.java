package com.x8academy.delivery_service.address;

import com.x8academy.delivery_service.utility.Validator;

import java.util.Objects;

import static java.lang.Math.atan2;
import static java.lang.Math.cos;
import static java.lang.Math.pow;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;
import static java.lang.Math.toRadians;

/**
 * Imitates a real life address. The address has a country, city, street name,
 * street number apartment number and a geographical location.
 */
public final class Address {
    private final Country country;
    private final City city;
    private final String streetName;
    private final int streetNumber;
    private final int appNumber;
    private final Location location;

    /**
     * Creates an Address object.
     * Country name must not be null or empty and meet the Validator
     * requirements.
     * City name must not be null or empty and meet the Validator requirements.
     * Street name must not be null or empty and meet teh Validator
     * requirements.
     * Street number can't be below 1.
     * Apartment number can't be below 1.
     *
     * @param country      the name of the country.
     * @param city         the name of the city.
     * @param streetName   street name.
     * @param streetNumber street number.
     * @param appNumber    apartment number.
     */
    public Address(final Country country, final City city,
                   final String streetName, final int streetNumber,
                   final int appNumber) {

        assert Validator.verifyCountryExists(country)
                : "Country doesn't exist.";
        assert Validator.verifyCityExists(city) : "City doesn't exist.";
        assert Validator.verifyNaming(streetName) : "Street name can't null "
                + "or empty and must follow the country naming convention.";
        assert streetNumber >= 1 : "Street number cant be below 1.";
        assert appNumber >= 1 : "Apartment number can't be below 1.";

        this.country = country;
        this.city = city;
        this.streetName = streetName;
        this.streetNumber = streetNumber;
        this.appNumber = appNumber;
        this.location = findLocation();

    }

    /**
     * Calculates the distance to another address.
     *
     * @param destinationAddress the address to which you want to calculate the
     *                           distance.
     * @return the distance between the two addresses.
     */
    public int getDistanceTo(final Address destinationAddress) {
        return this.getLocation().distanceTo(destinationAddress.getLocation());
    }

    /**
     * Returns the name of the country from the address.
     *
     * @return {@link #country}
     */
    public Country getCountry() {
        return this.country;
    }

    /**
     * Returns the name of the city from the address.
     *
     * @return {@link #city}
     */
    public City getCity() {
        return this.city;
    }

    /**
     * Returns the street name from the address.
     *
     * @return {@link #streetName}
     */
    public String getStreetName() {
        return this.streetName;
    }

    /**
     * Returns the street number from the address.
     *
     * @return {@link #streetNumber}
     */
    public int getStreetNumber() {
        return this.streetNumber;
    }

    /**
     * The apartment number from the address.
     *
     * @return {{@link #appNumber}}
     */
    public int getAppNumber() {
        return this.appNumber;
    }

    /**
     * Returns the geographical coordinates from the address.
     *
     * @return {@link #location}
     */
    public Location getLocation() {
        return this.location;
    }

    @Override
    public String toString() {
        return this.country + ", " + this.city + this.streetName + ", "
                + this.streetNumber + ", " + this.appNumber;
    }

    private Location findLocation() {
        //insert implementation here.
        return new Location(0, 0);
    }

    /**
     * This class functions as a real world location,
     * when given latitude and longitude point values.
     */
    private static final class Location {

        private static final double MIN_LATITUDE = -90;
        private static final double MAX_LATITUDE = 90;
        private static final double MIN_LONGITUDE = -180;
        private static final double MAX_LONGITUDE = 180;

        private final double latitude;
        private final double longitude;

        /**
         * Creates and Object that contains two fields -
         * latitude and longitude used to define the
         * position of an object in real world
         * coordinates.
         *
         * @param latitude  the north-south coordinate of a given point, its
         *                  value must be between -90 and 90 {@link #latitude}
         * @param longitude the east-west coordinate of a given point, its value
         *                  must be between -180 and 180 {@link #longitude}
         */
        private Location(final double latitude, final double longitude) {
            assert Validator.verifyIntervalBelonging(
                    latitude, MIN_LATITUDE, MAX_LATITUDE)
                    : "Incorrect value for latitude.";
            assert Validator.verifyIntervalBelonging(
                    longitude, MIN_LONGITUDE, MAX_LONGITUDE)
                    : "Incorrect value for longitude.";

            this.latitude = latitude;
            this.longitude = longitude;
        }

        private static final double EARTH_RADIUS_IN_KILOMETERS = 6371;

        /**
         * Calculates the distance between two
         * Location objects.
         * Uses the haversine formula.
         *
         * @param other Location object.
         * @return the distance between the first
         * and the second Location object.
         */
        public int distanceTo(final Location other) {
            assert other != null : "The other location shouldn't be null";

            final double distLatRad = toRadians(other.latitude - this.latitude);
            final double distLongRad = toRadians(
                    other.longitude - this.longitude
            );
            final double a = pow(sin(distLatRad / 2), 2)
                    + cos(toRadians(this.latitude))
                    * cos(toRadians(other.latitude))
                    * pow(sin(distLongRad / 2), 2);
            final double c = 2 * atan2(sqrt(a), sqrt(1 - a));

            return (int) (EARTH_RADIUS_IN_KILOMETERS * c);
        }

        /**
         * Get the north-south coordinates of a geographical point.
         *
         * @return {@link #latitude}
         */
        public double getLatitude() {
            return latitude;
        }

        /**
         * Get the east-west coordinates of a geographical point.
         *
         * @return {@link #longitude}
         */
        public double getLongitude() {
            return longitude;
        }

        /**
         * Prints all the information about the object.
         *
         * @return {@link #latitude} and {@link #longitude}
         */
        @Override
        public String toString() {
            return this.latitude + " " + this.longitude;
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            final Location location = (Location) o;

            return Double.compare(location.latitude, latitude) == 0
                    && Double.compare(location.longitude, longitude) == 0;
        }

        @Override
        public int hashCode() {
            return Objects.hash(latitude, longitude);
        }
    }
}
