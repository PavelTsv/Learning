package com.x8academy.delivery_service.address;

/**
 * Created by pavel on 11.10.17.
 */
public enum Country {
    /**
     * Country Bulgaria.
     */
    Bulgaria,
    /**
     * Country Germany.
     */
    Germany
}
