package com.x8academy.delivery_service.company;

/**
 * Item to be delivered from one address to another.
 */
public enum Item {
    /**
     * The item that is getting delivered.
     */
    ITEM
}
