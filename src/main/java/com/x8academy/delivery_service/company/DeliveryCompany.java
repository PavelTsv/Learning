package com.x8academy.delivery_service.company;

import com.x8academy.delivery_service.address.Address;
import com.x8academy.delivery_service.person.DeliveryPerson;
import com.x8academy.delivery_service.utility.Validator;

import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents real life delivery service company. The company has a name,
 * country and budget.
 */
public final class DeliveryCompany {

    private static final int OFFICE_BUILD_COST = 50_000;

    private final String name;
    private final Address address;

    private Long budget;

    private final List<Office> offices = new ArrayList<>();

    /**
     * Creates a DeliveryCompany object.
     * Company name must follow the Validator rules.
     * Address can't be null.
     * Company budget must be equal or above zero.
     *
     * @param name    the name of the delivery company.
     * @param address the address of the delivery company.
     * @param budget  the budget of the delivery company.
     */
    public DeliveryCompany(final String name, final Address address,
                           final Long budget) {

        assert Validator.isValidCompanyName(name) : "Illegal company name.";
        assert address != null : "Address is null.";
        assert budget >= 0 : "Company budget can't be negative.";

        this.name = name;
        this.address = address;
        this.budget = budget;
    }

    /**
     * For the given office, check if hiring a new employee will increase the
     * monthly earnings.
     * office must not be null and must be a part of the company.
     * newEmployee must not be null.
     *
     * @param office      the office at which we want to see if hiring one more
     *                    employee will increase the monthly earnings.
     * @param newEmployee the new employee to be checked if it will increase
     *                    the office earnings.
     * @return true if the office will earn more money with one more employee.
     */
    public boolean worthHiring(
            final Office office, final DeliveryPerson newEmployee) {

        assert office != null && offices.contains(office)
                : "office cant be null and must be a part of the company.";
        assert newEmployee != null : "The employee cant be null";

        final long officeMonthlyEarnings =
                office.getMonthlyEarnings() - office.getMonthlyEmployeeSalary();

        office.hire(newEmployee);
        final long officeMonthlyEarningsWithOneMoreEmployee =
                office.getMonthlyEarnings() - office.getMonthlyEmployeeSalary();
        office.fire(newEmployee);

        return officeMonthlyEarningsWithOneMoreEmployee > officeMonthlyEarnings;
    }

    /**
     * Creates an Office object and adds it to the company's list of offices.
     * Address cant be null.
     * The expected daily deliveries must be a number above zero.
     * Opening a new office must meet the company budget.
     *
     * @param officeAddress            the address of the new office.
     * @param expectedDeliveriesPerDay the amount of daily deliveries the
     *                                 office has to deal with.
     */
    public void openNewOffice(final Address officeAddress,
                              final int expectedDeliveriesPerDay) {

        assert officeAddress != null : "address can't be null.";
        assert expectedDeliveriesPerDay > 0
                : "Deliveries per day must be above 0";
        assert budget - OFFICE_BUILD_COST > 0
                : "Company doesn't have enough money to open a new office";

        budget -= OFFICE_BUILD_COST;

        offices.add(new Office(officeAddress, expectedDeliveriesPerDay));
    }

    /**
     * Removes an office from the companies list.
     * Office must not be null and must be part of the companies line.
     *
     * @param office the office you want to remove from the company.
     */
    public void removeOffice(final Office office) {

        assert office != null && offices.contains(office);

        offices.remove(office);
    }

    /**
     * Returns the name of the company.
     *
     * @return {@link #name}
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the address of the company.
     *
     * @return {@link #address}
     */
    public Address getAddress() {
        return address;
    }


    private boolean hasOfficeInCity(final String city) {

        assert Validator.verifyNaming(city) : "Invalid city name.";

        for (Office office : offices) {
            if (office.address.getCity().equals(city)) {
                return true;
            }
        }
        return false;
    }

    /**
     * The office class is used to imitate real world working office of a
     * delivery company. The office has
     */
    private final class Office {

        private final Address address;
        private final int expectedDeliveriesPerDay;

        private final List<DeliveryPerson> listOfEmployees = new ArrayList<>();

        /**
         * Creates an Office object.
         * city must meet the Validator city naming rules.
         * expectedDeliveriesPerDay must be above zero.
         *
         * @param address                  the address of the new office.
         * @param expectedDeliveriesPerDay the amount of deliveries the company
         *                                 expects to make daily.
         */
        private Office(final Address address,
                       final int expectedDeliveriesPerDay) {

            assert address != null : "address cant be null.";
            assert expectedDeliveriesPerDay > 0
                    : "expectedDeliveriesPerDay must be above zero.";

            this.address = address;
            this.expectedDeliveriesPerDay = expectedDeliveriesPerDay;
        }

        /**
         * The office hires a new employee.
         * employee must not be equal to null.
         *
         * @param employee the employee getting hired.
         */
        public void hire(final DeliveryPerson employee) {

            assert employee != null;

            listOfEmployees.add(employee);
        }

        /**
         * Removes an employee from the company.
         * The employee must not be null and must be a part of the company.
         *
         * @param employee the employee getting fired.
         */
        public void fire(final DeliveryPerson employee) {

            assert employee != null && listOfEmployees.contains(employee);

            listOfEmployees.remove(employee);
        }

        private static final double AVERAGE_DELIVERY_PRICE_PER_EMPLOYEE = 0.5;

        /**
         * Calculates the delivery price when considering only the amount of
         * employees working at the given day.
         * listOfEmployeesWorkingAtGivenDay needs to be above zero and not null;
         *
         * @param listOfEmployeesWorkingAtGivenDay the employees working at the
         *                                         given day.
         * @return delivery price.
         */
        public double calculateDeliveryPriceForGivenDay(
                final List<DeliveryPerson> listOfEmployeesWorkingAtGivenDay) {

            assert listOfEmployeesWorkingAtGivenDay.size() != 0
                    && listOfEmployeesWorkingAtGivenDay != null
                    : "No employee work at given day.";

            return 1 + listOfEmployeesWorkingAtGivenDay.size()
                    * AVERAGE_DELIVERY_PRICE_PER_EMPLOYEE;
        }

        private static final int EXPECTED_DAYS_DELIVERY_TIME = 14;
        private static final int EXPECTED_DAYS_SINGLE_EMPLOYEE = 1;

        /**
         * Returns the delivery time for the given day.
         * day must not be null.
         *
         * @param day the given day of the week
         * @return the expected delivery time.
         */
        public int getDeliveryTime(final DayOfWeek day) {
            assert listOfEmployees.size() != 0
                    : "No employees working at the given ";
            assert day != null : "Invalid day of the week";

            final int employeesWorkingAtGivenDay =
                    getEmployeesWorkingOn(day).size();
            final int deliveryTime =
                    (int) Math.floor(EXPECTED_DAYS_DELIVERY_TIME
                            / employeesWorkingAtGivenDay);

            return Math.min(EXPECTED_DAYS_SINGLE_EMPLOYEE, deliveryTime);
        }

        /**
         * Calculates the amount of money the employees get per month.
         *
         * @return the the sum of all employee's salaries.
         */
        public long getMonthlyEmployeeSalary() {
            long monthlyPayment = 0;

            for (DeliveryPerson employee : listOfEmployees) {
                monthlyPayment += employee.getSalary();
            }

            return monthlyPayment;
        }

        private static final int NUMBER_OF_WEEKS_IN_MONTH = 4;

        /**
         * The amount of money the office earns from deliveries.
         *
         * @return total money earned from deliveries.
         */
        public long getMonthlyEarnings() {
            long monthlyEarnings = 0;

            for (int i = 0; i < NUMBER_OF_WEEKS_IN_MONTH; i++) {
                for (DayOfWeek day : DayOfWeek.values()) {
                    monthlyEarnings
                            += calculateDeliveryPriceForGivenDay(
                            getEmployeesWorkingOn(day))

                            * expectedDeliveriesPerDay;
                }
            }
            return monthlyEarnings;
        }

        /**
         * Returns teh address of the office.
         *
         * @return {@link #address}
         */
        public Address getAddress() {
            return address;
        }

        private List<DeliveryPerson> getEmployeesWorkingOn(
                final DayOfWeek day) {

            assert day != null : "Invalid day.";

            final List<DeliveryPerson> atGivenDayWorkingEmployees =
                    new ArrayList<>();

            for (DeliveryPerson employee : listOfEmployees) {
                if (employee.getWorkingDays().contains(day)) {
                    atGivenDayWorkingEmployees.add(employee);
                }
            }
            return atGivenDayWorkingEmployees;
        }
    }
}
