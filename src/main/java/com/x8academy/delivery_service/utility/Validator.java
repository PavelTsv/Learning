package com.x8academy.delivery_service.utility;

import com.x8academy.delivery_service.address.City;
import com.x8academy.delivery_service.address.Country;

/**
 * Created by pavel on 03.10.17.
 */
public final class Validator {

    private static final String COMPANY_NAME
            = "^[A-Z]([a-zA-Z0-9]|[- @\\.#&!])*$";
    private static final String NAME_REGEX = "^[a-zA-Z]+(?:[\\s-][a-zA-Z]+)*$";

    private Validator() {

    }

    /**
     * Checks if the given name matches the company name rules.
     *
     * @param name the name of the company.
     * @return true if not null or empty and if it matches the rules.
     */
    public static boolean isValidCompanyName(final String name) {
        return verifyNotNullAndNotEmpty(name)
                && name.matches(COMPANY_NAME);
    }

    /**
     * Verifies by the given regex
     * if the naming convention is met.
     *
     * @param name real world name
     * @return true if the above condition
     * is met
     */
    public static boolean verifyNaming(final String name) {
        return verifyNotNullAndNotEmpty(name) && name.matches(NAME_REGEX);
    }

    /**
     * This function verifies that the given double number is bigger or equal
     * to minValue and smaller or equal to maxValue.
     *
     * @param value    the number that you want to verify.
     * @param minValue the min value that you want to compare number to.
     * @param maxValue the max value that you wan to compare number to.
     * @return true if value is between minValue and maxValue.
     */
    public static boolean verifyIntervalBelonging(final double value,
                                                  final double minValue,
                                                  final double maxValue) {

        return value >= minValue && value <= maxValue;
    }

    /**
     * Verifies that the given country exists.
     *
     * @param country the name of the country.
     * @return true if the country exists.
     */
    public static boolean verifyCountryExists(final Country country) {
        return false;
    }

    /**
     * Verifies that the given city exists.
     *
     * @param city the name of the city.
     * @return true if the city exists.
     */
    public static boolean verifyCityExists(final City city) {

        return false;
    }

    private static boolean verifyNotNullAndNotEmpty(final String string) {
        return !(string == null || string.equals(""));
    }


}
